import { Response } from '../model/Api';
import { Todo } from '../model/Todo';

const API = 'http://localhost:3000/api/v1/todo';
/**
 * TODO: add typing to fetch response on create, update, delete
 * example in `fetchAll`
 */

async function remove(id: string): Promise<any> {
  return fetch(`${API}/${id}`, { method: 'DELETE' });
}

async function update(todo: Todo): Promise<any> {
  return fetch(`${API}/${todo._id}`, {
    method: 'PUT',
    body: JSON.stringify(todo),
  });
}

async function create(description: string): Promise<any> {
  return fetch(`${API}`, {
    method: 'POST',
    body: JSON.stringify({ description }),
  });
}

async function fetchData<T>(url: string): Promise<T> {
  const response = await fetch(url);
  // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
  const body = await response.json();

  // eslint-disable-next-line @typescript-eslint/no-unsafe-return
  return body;
}

async function fetchAll(offset: number): Promise<Response> {
  // eslint-disable-next-line no-underscore-dangle
  return fetchData<Response>(`${API}?offset=${offset}`);
}

export { remove, update, create, fetchAll };
