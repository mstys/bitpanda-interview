import { Todo } from './Todo';

export interface Meta {
  offset: number;
  hasNextPage?: boolean;
  hasPrevPage?: boolean;
}

export interface Response {
  items: Todo[];
  meta: Meta;
}
