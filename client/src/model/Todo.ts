export interface Todo {
  _id: string;
  description: string;
  done: boolean;
  updatedAt: string,
  createdAt: string,
}
