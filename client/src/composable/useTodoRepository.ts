import { Ref, ref, onMounted } from '@vue/composition-api';

import { remove, update, create, fetchAll } from '../api/todo';
import { Meta, Response } from '../model/Api';
import { Todo } from '../model/Todo';

/**
 * TODO: return Promise in every server call
 * example: createTodo
 */
interface TodoRepository {
  items: Ref<Todo[]>;
  meta: Ref<Meta>;
  setAsDone(todo: Todo): void;
  createTodo(description: string): Promise<boolean>;
  removeTodo(id: string): Promise<any>;
  nextPage(): void;
  prevPage(): void;
}

export default function useTodoRepository(): TodoRepository {
  let response: Response;
  const items = ref<Todo[]>([]);
  const meta = ref<Meta>({
    offset: 0,
  });

  const getData = async (offset = 0) => {
    try {
      response = await fetchAll(offset);
      items.value = response.items;
      meta.value = response.meta;
    } catch (e) {
      console.error(e);
    }
  };

  const setAsDone = async (todo: Todo) => {
    try {
      await update({
        ...todo,
        done: !todo.done,
      });
      await getData(+meta.value.offset);
    } catch (e) {
      console.error(e);
    }
  };

  const createTodo = async (description: string): Promise<boolean> => {
    try {
      await create(description);
      await getData(+meta.value.offset);

      return Promise.resolve(true);
    } catch (e) {
      throw new Error(e);
    }
  };

  const nextPage = async () => {
    await getData(+meta.value.offset + 20);
  };

  const prevPage = async () => {
    if (meta.value.offset > 0) {
      await getData(+meta.value.offset - 20);
    }
  };

  const removeTodo = async (id: string): Promise<any> => {
    try {
      await remove(id);
      await getData(+meta.value.offset);
    } catch (e) {
      console.error(e);
    }
  };

  onMounted(async () => {
    await getData();
  });

  return {
    items,
    meta,
    setAsDone,
    createTodo,
    removeTodo,
    nextPage,
    prevPage,
  };
}
